call plug#begin('~/.local/share/nvim/plugged')

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'majutsushi/tagbar'
Plug 'sbdchd/neoformat'
Plug 'pangloss/vim-javascript'
Plug 'prettier/vim-prettier'
Plug 'airblade/vim-rooter'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'airblade/vim-gitgutter'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tpope/vim-fugitive'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'omnisharp/omnisharp-vim'
Plug 'junegunn/fzf.vim'

call plug#end()

set mouse=a
set tabstop=8
set expandtab
set shiftwidth=4
set autoindent
set smartindent
set cindent
set viminfo=

let g:airline_powerline_fonts = 1
let g:airline_theme='angr'
let g:airline#extensions#tabline#enabled = 1

colorscheme delek

set statusline+=%#warningmsg#
set statusline+=%*

let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : ">",
    \ "Staged"    : "+",
    \ "Untracked" : "u",
    \ "Renamed"   : "r",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "-",
    \ "Dirty"     : ">",
    \ "Clean"     : "!",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?"
    \ }

function BaseIDE()
	execute ":NERDTree"
	execute ":Tagbar"
	execute ":wincmd p"
	execute ":edit"
endfunction

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
autocmd VimEnter * if &filetype == "java" | call BaseIDE() | endif
autocmd VimEnter * if &filetype == "cs" | call BaseIDE() | endif
