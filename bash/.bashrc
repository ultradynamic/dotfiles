# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# load common shell configs (e.g. aliases)
source ~/.shrc

# git completion
source ~/script/git/git-completion.bash
source ~/script/git/git-prompt.sh
export GIT_PS1_SHOWDIRTYSTATE=1
export GIT_PS1_SHOWSTASHSTATE=1
export GIT_PS1_SHOWUNTRACKEDFILES=1
export GIT_PS1_SHOWUPSTREAM="auto"

# colored prompt
PS1='\[${GREEN}\]\u\[${MAGENTA}\]@\[${WHITE}\]\h\[${BLUE}\] \[${BLUE}\]\w\[${RED}\]$(__git_ps1 " (%s)")\[${RESET}\] \[${GREEN}\]\$\[${RESET}\] '

# display a fortune
fortune
