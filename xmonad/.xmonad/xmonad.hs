import XMonad
import XMonad.Util.Run
import XMonad.Layout.Spacing
import XMonad.Layout.SimpleFloat
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.Place
import XMonad.Util.Cursor
import XMonad.Util.EZConfig
import XMonad.Layout.SimpleDecoration
import XMonad.Actions.WithAll
import System.Exit

myLayout = avoidStruts (tiled ||| Full)
  where
    tiled = spacing 5 $ Tall nmaster delta ratio
    nmaster = 1
    ratio = 1/2
    delta = 3/100

myTerminal = "termite"
myBrowser = "firefox -width 640 -height 480 -private ~/.browser/home.html"
myFileManager = "thunar"
myCloud = myTerminal ++  " -e ~/script/cloud.sh"
myLockCommand = "xscreensaver-command -lock"
mySleepCommand = "xscreensaver-command -lock && sleep 5 && systemctl suspend"

bar_font = "'artwiz lime'-8"
colour1 = "#849DA2"
colour2 = "#CC0052"
x_val = "306"
width = "1060"

myXMonadBar = "dzen2 -dock -x '0' -y '0' -h '14' -w '" ++ x_val ++ "' -ta 'l' -fg '" ++ colour1 ++ "' -bg '#000000' -fn " ++ bar_font
myStatusBar = "conky -c ~/.conky/conkyrc | dzen2 -dock -x '" ++ x_val ++ "' -y '0' -h '14' -w '" ++ width ++ "' -ta 'r' -fg '" ++ colour1 ++ "' -bg '#000000' -fn " ++ bar_font

myPlacement = inBounds (underMouse (0.5,0.5))

main = do
    dzenLeftBar <- spawnPipe myXMonadBar
    dzenRightBar <- spawnPipe myStatusBar
    xmonad $ defaultConfig
      { terminal    = myTerminal
      , modMask     = mod4Mask
      , layoutHook  = myLayout
      , focusedBorderColor = colour2
      , normalBorderColor = colour1
      , borderWidth = 1
      , manageHook = placeHook myPlacement <+> doFloat <+> manageDocks <+> manageHook defaultConfig
      , handleEventHook = handleEventHook defaultConfig <+> docksEventHook
      , logHook = dynamicLogWithPP $ defaultPP
          { ppOutput = hPutStrLn dzenLeftBar
          }
      , startupHook = setDefaultCursor xC_left_ptr <+> docksStartupHook
      } `additionalKeys`
         [ ((mod4Mask .|. shiftMask, xK_l), spawn myLockCommand)
         , ((mod4Mask .|. shiftMask, xK_s), spawn mySleepCommand)
         , ((mod4Mask .|. shiftMask, xK_t), spawn myCloud)
         , ((mod4Mask , xK_b), spawn myBrowser)
         , ((mod4Mask, xK_y), spawn myFileManager)
         , ((mod4Mask , xK_p), spawn "~/script/ui/dmenu_run")
         , ((mod4Mask .|. shiftMask, xK_m), spawn "~/script/ui/mpd.sh toggle")
         , ((mod4Mask .|. shiftMask, xK_n), spawn "~/script/ui/mpd.sh next")
         , ((mod4Mask .|. shiftMask, xK_b), spawn "~/script/ui/mpd.sh prev")
         , ((mod4Mask , xK_e), spawn myTerminal)
         , ((mod4Mask , xK_s), sinkAll)
         , ((mod4Mask , xK_q), kill)
         , ((mod4Mask .|. shiftMask, xK_q), killAll)
         , ((mod4Mask , xK_x), restart "xmonad" True)
         , ((mod4Mask .|. shiftMask, xK_x), io (exitWith ExitSuccess))
         , ((mod4Mask , xK_f), withAll(\w -> float w))
         ]
